import string
import random
import refresh
from apiclient.discovery import build
from datetime import date
import gdata
from  gdata.spreadsheets.client import CellQuery as cellquery
import gdata.spreadsheets.client as spreadsheets_client
import gdata.spreadsheets.data as data


def config(configfile,varname): 
    try: 
        with open(configfile,'r') as config: 
            for line in config: 
                if varname in line: 
                    value=line.split('=')[1].strip() 
                    return value 
    except: 
        return None 

def xml_ok(colstring): 
    return colstring.strip().replace('&','').replace(' ','').replace('#','no').replace('%','perc').replace('-','').lower() 

def belist(arg,splitchar = None):
    if not isinstance(arg,list):
        if not splitchar: 
            return [arg]
        return arg.split(splitchar)
    return arg

def getlast(instring,sep = '/'): 
    try:
        return instring.rsplit(sep,1)[1]
    except:
        return None

class searchby(dict): 
    '''searching helper class
       Description: Dictionary of key value pairs to search for 
       in spreadsheet rows. 
       Inputs:
           colname, value: initial column name and value to search for.
           
           date_column_title: name of the column the date will be found in.
           If date_column_title is set then the month and year must be set. If
           these are set then the row will be marked true for this parameter if
           the date in the column is within the month,year specified. Currently
           only works if the date format in the column is mm/dd/yyyy 
           month: month (1 to 12) to search for
           year: year to search for
           
           allmatch: if true found method returns true only if all key,value 
           pairs match in the row. If allmatch is false found method returns
           true if any key value pairs match 

    '''
    def __init__(self,colname = None, value = None, 
                 date_column_title = None,month = None,
                 year = None,allmatch = True): 
        self.value = value 
        self.date_column_title = date_column_title
        self.month = month
        self.year = year
        self.allmatch = allmatch
        if colname: 
            self.add(colname, value)
    
    @property
    def date_column_title(self):
        return self._date_column_title

    @date_column_title.setter
    def date_column_title(self,value):
        try: 
            self._date_column_title = xml_ok(value)
        except:
            self._date_column_title = None
    
    def add(self,key,value):
        self[xml_ok(key)] = value

    
    def found(self,row):
        date_ok = self.allmatch
        if self.date_column_title:
            try:
                invalue = row.get_value(self.date_column_title)
                testdate = invalue.split(' ')[0]
                (tmonth,day,tyear) = testdate.split('/')
                date_ok = self.inmonth(int(tmonth),int(tyear))
            except:
                date_ok = False 
        searchvalues = [row.get_value(k) == v for k,v in self.items()] + [date_ok]
        if self.allmatch:
            return all(searchvalues)
        return any(searchvalues)
    
    @property
    def month(self):
        return self._month

    @month.setter
    def month(self,value):
        try:
            if 0 < int(value) < 13:
                self._month = int(value)
                return
        except:
            self._month = None

    @property
    def year(self):
        return self._year

    @year.setter
    def year(self,value):
        try:
            if 1980 < int(value) < 2030:
                self._year = int(value)
                return
        except:
            self._year = None

    def inmonth(self,month,year):
        return int(month) == self.month and int(year) == self.year

class SDMGoogleSheet(object):
    '''Main Spreadsheet class.
    Inputs:
        configfile = full path and title to config file with the following 
                    information and format: (minimally):
                        OAuthCredentials=/path/to/OAuthcredentials.txt
                        client_secrets=/path/to/client_secrets.json
        title: Title of the spreadsheet. 
        worksheet_name: Name of the worksheet within the spreadsheet
        readonly: If True will not allow creation of or writing to 
                  spreadsheets or worksheets.Default: False
    '''
    def __init__(self,configfile = None, 
                 title = None, worksheet_name = None,
                 readonly = False,
                 columns = None):
        self.readonly = readonly
        self.worksheet_name = worksheet_name
        self.configfile = configfile
        self.title = title
        self.columns = columns
        self.current_worksheet = None
 
    @property
    def columns(self): 
        return self._columns

    @columns.setter
    def columns(self,columns):
        splitchar = ","
        self._columns = belist(columns,splitchar)

    @property
    def columnheaders(self):
        try:
            return  ','.join([xml_ok(c) for c in self.columns])
        except:
            return None

    def find_spreadsheet_key(self):
        try:
            return [getlast(spreadsheet.id.text) for spreadsheet in 
                    self.all_spreadsheets if 
                    xml_ok(spreadsheet.title.text) == xml_ok(self.title)][0]
        except Exception as e:
            print "exception in find spreadsheet key: {}".format(repr(e))
            return None

    @property
    def spreadsheet_key(self):
        try:
            return self._spreadsheet_key 
        except Exception as e:
            self._spreadsheet_key = self.find_spreadsheet_key()
        if not self._spreadsheet_key:
            '''this is a new spreadsheet'''
            if not self._spreadsheet_key: 
                self._spreadsheet_key = self.create_new_spreadsheet()
                if self.columns: 
                    self.add_columns()
                if self.worksheet_name: 
                    self.rename_worksheet()
        return self._spreadsheet_key

    @property
    def authfile(self):
        try:
            return self._authfile
        except:
            return config(self.configfile,'OAuthCredentials')
    
    @property
    def client_secrets(self):
        try:
            return self._client_secrets
        except:
            return config(self.configfile,'client_secrets')
    
    @property
    def authtoken(self):
        return refresh.authorized(authfile=self.authfile,
                                  secrets = self.client_secrets)
   
    @property
    def credentials(self):
        return refresh.get_credentials(authorization_file = self.authfile,
                                       secrets = self.client_secrets)
    @property
    def client(self):
        spr_client = spreadsheets_client.SpreadsheetsClient()
        spr_client.auth_token = gdata.gauth.OAuth2TokenFromCredentials(self.credentials)
        return spr_client 
    
    @property
    def current_worksheet(self):
        try: 
            if self._current_worksheet:
                return self._current_worksheet
        except:
            pass 
        self.current_worksheet = None 
        return self._current_worksheet

    @property
    def current_worksheet_id(self): 
        return getlast(self.current_worksheet.id.text)
    
    @property
    def current_worksheet_title(self):
        return self.current_worksheet.title.text

    @current_worksheet.setter
    def current_worksheet(self,worksheet_identifier = None):
        '''this is what causes the new spreadsheet to be created if it asks for
            a spread sheet key and can't find one '''
        try:
            spreadsheet_key = self.spreadsheet_key
        except Exception as e:
            print "exc is {}".format(repr(e))
            exit()
        try:
            if worksheet_identifier == self._current_worksheet_title: 
                return
        except:
            pass
        if not worksheet_identifier:
            try:
                self._current_worksheet = self.all_worksheets[0]
                return
            except:
                pass
        try:
            self._current_worksheet = self.find_worksheet(worksheet_identifier)
            if self._current_worksheet:
                return 
        except:
            pass
        if not self.spreadsheet_key: 
            self.create_new_worksheet(worksheet_identifier)

    @property
    def all_worksheets(self):
        try:
            return self.client.get_worksheets(self.spreadsheet_key).entry
        except:
            return None

    @property
    def all_rows(self):
        return self.client.GetListFeed(self.spreadsheet_key,
                                       self.current_worksheet_id).entry
   
    @property
    def all_spreadsheets(self):
        return self.client.get_spreadsheets().entry

    def create_new_spreadsheet(self):
        if self.readonly:
            return False
        print self.authfile
        http = refresh.authorized(authfile = self.authfile,
                                  secrets = self.client_secrets)
        try:
            print "gooing to make drive"
            drive_service = build('drive', 'v2', http=http)
            print drive_service
            body = {'mimeType': 'application/vnd.google-apps.spreadsheet', 
                    'title': self.title, } 
            files = drive_service.files().insert(body=body).execute(http=http)
            return files['id']
        except Exception as e:
            print 'drive service returned {}'.format(repr(e))
            exit()

    @property
    def colcount(self):
        return len(self.columns)
    
    def rename_worksheet(self,newtitle = None,current_title = None):
        if self.readonly:
            return False
        if not newtitle:
            newtitle = self.worksheet_name
        if self.find_worksheet(newtitle):
            return False
        if current_title:
            self.current_worksheet = current_title
        self.current_worksheet.get_elements(tag='title')[0].text = newtitle
        self.client.update(self.current_worksheet,force = True)
        self.current_worksheet = newtitle

    def find_worksheet(self,worksheet_name):
        try:
            return [s for s in self.all_worksheets 
                    if s.title.text == worksheet_name][0]
        except:
            return None

    def create_new_worksheet(self,worksheet_name = None,
                             columns = None,splitchar = ","):
        if self.readonly:
            return False
        if not columns:
            columns = self.columns
        else:
            columns = belist(columns,splitchar)
        if columns: 
            colcount = len(columns)
        else: colcount = 0
        if not worksheet_name:
            worksheet_name = self.worksheet_name
        if not worksheet_name:
            worksheet_name = ''.join([random.choice(string.letters) for a in
                                                   range(7)])
        worksheet = self.find_worksheet(worksheet_name)
        if worksheet:
            self._current_worksheet = worksheet
            return
        worksheet = self.client.add_worksheet(spreadsheet_key = self.spreadsheet_key, 
                                                 title = worksheet_name,
                                                 rows = 1, 
                                                 cols= colcount)
        self.add_columns(columns,worksheet)
        
    def add_columns(self,columns = None,worksheet = None):
        if not columns:
            columns = self.columns
        if not worksheet:
            worksheet = self.current_worksheet
        if worksheet: 
            worksheet_id = getlast(worksheet.id.text)
        '''todo: use batch method '''
        cell_query = cellquery(
            min_row = 1, max_row = 1, 
            min_col = 1, max_col = len(columns),
            return_empty = True) 
        cells = self.client.GetCells(self.spreadsheet_key, 
                                     worksheet_id, 
                                     q = cell_query)
        for col,val in enumerate(columns):
            cell_entry = cells.entry[col - 1]
            cell_entry.cell.input_value = val
            self.client.update(cell_entry)
        self._current_worksheet = worksheet

    def create_new_table(self,table_name):
        return 
    
    def convert_headers(self,header):
        return xml_ok(header)
       
    def write_headers(self):
        if self.readonly:
            return False
        send_data = {column:'' for column in self.columns}
        self.write_to_worksheet(send_data)

    def batch_write_to_worksheet(self,list_of_row_data,checkexists = ''):
        if self.readonly:
            return False
        for row_data in list_of_row_data:
            if checkexists:
                if not self.value_exists(checkexists,row_data[checkexists]): 
                    self.write_to_worksheet(row_data)
            else: self.write_to_worksheet(row_data)
                
    def write_to_worksheet(self,row_data): 
        if self.readonly:
            return False
        send_data = {self.convert_headers(key):'{}'.format(value) for key,value in
                     row_data.items()}
        '''create an empty ListEntry,fill it up, and add it to the worksheet'''
        entry = data.ListEntry()
        for k,v in send_data.items():
            entry.set_value(k,v)
        self.client.add_list_entry(entry,
                                   self.spreadsheet_key,
                                   self.current_worksheet_id)
    
    def read_from_table(self):
        pass 
    
    def write_to_table(self):
       pass 
   
    def column_value(self,row,columnname): 
        value = row.get_value(columnname)
        if not value:
            return 0
        return value
    
    def value_exists(self,column,value):
        rows = self.client.GetListFeed(self.spreadsheet_key,self.current_worksheet_id).entry
        value = '{}'.format(value).strip()
        for row in rows: 
            if row.get_value(xml_ok(column)).strip() == value:
                return True
        return False

    def get_by_col_value(self,returnlist, searchdict = None, date_title = None,
                         month = None, year = None,worksheet_name = None):
        returnlist = belist(returnlist)
        search = searchby()
        s = [search.add(k,v) for k,v in searchdict.items()]
        if date_title and month and year:
            search.date_column_title = date_title
            search.month = month
            search.year = year 
        if worksheet_name: 
            self.current_worksheet = worksheet_name 
        return [{column_name:self.column_value(row,xml_ok(column_name)) for column_name in returnlist} 
                for row in self.all_rows if search.found(row)]
    
def main():
    '''
    Examples:
        Make a new spreadsheet.
            If a spreadsheet with the same title exists 
            that spreadsheet will be returned. Otherwise 
            a new spreadsheet will be created
    '''
    gs = SDMGoogleSheet(configfile = 'googleconfig',
                        title = 'Sunnydale Accounts 2',
                        worksheet_name = 'FakeWorksheet',
                        columns = 'EventDate,Location,Name',
                        readonly = False)

    '''Write to spreadsheet'''
    names = ['Anya','Willow','Xander','Giles','Dawn']
    dates = ['2/12/2001','2/12/2001','2/13/2001','2/23/2012','3/12/2001']
    locs = ['library','kitchen','classroom','dining room','backyard']
    rows = [{'EventDate':x,'Location':y,'Name':z} for x,y,z in
            zip(dates,locs,names)] 
    for row_data in rows: 
        gs.write_to_worksheet(row_data)
    
    '''add new worksheet'''
    gs.create_new_worksheet('Locations',columns = 'Library,Home,Color')
    
    '''add data to worksheet'''
    libs = ['Stacks','Lamps','Books']
    colors = ['red','blue','orange']
    home = ['stove','sink','Dishes']
    places = [{'Library':y, 'Home':z,'Color':x} for y,z,x in zip(libs,home,colors)]
    do = [gs.write_to_worksheet(place) for place in places]
    
    '''search by column values'''
    columns_to_return = ['Library','Home']
    searchby = {'Color':'red'}
    results = gs.get_by_col_value(returnlist = columns_to_return,
                        worksheet_name = 'Locations',
                        searchdict = searchby)
    '''search by column values and date range (in month)'''
    month = 2
    year = 2001
    date_title = 'EventDate'
    columns_to_return = ['Location','Name']
    searchfor = {'Name':'Anya'}
    results = gs.get_by_col_value(returnlist = columns_to_return, 
                                  worksheet_name = 'FakeWorksheet',
                                  searchdict = searchfor, 
                                  date_title = date_title, 
                                  month = month, 
                                  year = year)
    print results

if __name__ == "__main__":
    main()
    
