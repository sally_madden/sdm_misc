import httplib2
from oauth2client.client import OAuth2WebServerFlow
from oauth2client.file import Storage
from apiclient.discovery import build
import gdata.spreadsheets.client
import gdata.gauth

'''docs for how to create these here:
    https://developers.google.com/identity/protocols/OAuth2'''
Client_id='long string'
Client_secret='shorter string'
Scope='https://spreadsheets.google.com/feeds/ https://www.googleapis.com/auth/drive'
REDIRECT_URI = 'urn:ietf:wg:oauth:2.0:oob'
flow = OAuth2WebServerFlow(client_id=Client_id, 
                           client_secret=Client_secret,
                           scope=Scope, 
                           redirect_uri=REDIRECT_URI)

#retrieve if available
storage = Storage('OAuthcredentials.txt')
credentials = storage.get()
if  credentials is None: 
    #step 1 
    auth_uri = flow.step1_get_authorize_url() # Redirect the user to auth_uri 
    print 'Go to the following link in your browser: ' + auth_uri 
    code = raw_input('Enter verification code: ').strip() 
    #step 2 
    credentials = flow.step2_exchange(code) 
else: 
    print 'GDrive credentials are still current' 

#authorise 
http = httplib2.Http() 
http = credentials.authorize(http) 
print 'authorization completed' 
service = build('drive', 'v2', http=http) 
storage.put(credentials)
gd_client = gdata.spreadsheets.client.SpreadsheetsClient()
gd_client.auth_token = gdata.gauth.OAuth2TokenFromCredentials(credentials)
